let
  niklash =
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC+O2c5XNryJUCKs5glP2GvfihpNKEWxRyxV0mG4xjDpqRQU7lA8/g0uGWqMXqfqGpRORtNHgmA4cIiWBC7Qv6LA4tYPXYPPIPDipcG7FPTu3d2K6PXD2/RF9KOHl1LIxgzk6ExEMYSW8CYXnawJNvjQFK1naR1+TlmQO5K4lBgyXapBBBjLQeASsMoVQXhhrHgh8cKvRWfFyoicV43K0K/r9x/8UarzNQ8imI4JB1F4tpWjn8tzMGfV1wLhyy2pO2OjkOI77oZSEwquuKZpw54ybBsnTpXO5a1wDVtEESXn44PWYGneaZCPUoSmBlcGS66rioN7+2KgnMQ16+7cT3dQoiHgFBhJv6aimUAqZC+jhlPFnWeNhMtQa9dBGqXMX6g3Z3EoKNcvGac6mW0r/LjK3WsyYtZpFQJ109FbtqesWyYlPVmIkGeHtNuCeEkj5QF/qaql1J4znigTp4XPSZoAEXW5nQq20apjZG65zfAMaSbZK9u8jH4+k4aZuoKgVk= niklash";
  matias =
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKVXV+51+7Evucq9Qi9QCs2LugQii6AjvDfIg3u7oiOe";
  tuukka =
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC2c/FQ9M+4UXPSbwBNysLBPgkMI3Lsa1SBLlaOCE5oT4jhc1/oy1h9C/D8gkrnvmbGxpa8XAsp54a7nXWCWyVJBvX6VKFZ8EN5q7AciA0vtxOPYxwOsL9BWEZRfYcH2Eubz+cxntQcmGwuPTp95Jz0MMgOm7saUVTMujSCkFclQtpedAWb5KO+Wp3gu4PTBT3JV2Os5OiLxsnsgvyvBU23lB74b1vqIAwZS7skTS90T03AEenwR1kBeoBaMeadITvd18DDkvu5sAc/vSaXToXh+TkS1NXsWW356h1D2qrLjiveQFRXhqrgWrNhYI1au6ncC1XrmOSLVt2WGjhicYWLDKHD9M/WiKwAKuJnK8hK1OANFazKxJKSp32w40d5u+SpzkI+HUTkrvTDcERv9rwlIf3aN9+LZ6WPlnbYJie3FW/RfhoUA/fPxEtKQ+iMKMo4Id5L9VoH7dEIVawkaH9G+9mqVklDAMHo51BPoWiTkPWT2Kb4q5x7WGyV1eSQKvlgeDvEO+NANRuSfOnV4D9URdR1FHjykxqeK624Vg25SJ414acueYRgEOmnlkPtCNTN7P/Uj1EF4RAaj0EevT3pN2TeqDNHMUV2WBWNBsWW/t/dU6gJ7k4aiQ9hNxLqIKHDoiKBBus6Ri/0vDnHG8i3QfacF+t0ZnoSUTeJ7QJpUw== tuukk@desktop-tuukk";
  flaketorio =
    "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCv/1+cnTRf4sS3fG9KYrCIoBrqNYuEc8GCiRX6zxMoNPLYjJn5GUb28KZ772Dc8l/dtdAwRTCGAELj2gbfLgch3cxX9doizUqtUESKXFF24qWyzdyHgjM3OUqW+wbpDHjM5GtXuPgzUfimyQ7fB6f8ua9EOsNoxnmbmY7nlwJ1pRr09AlznIWGk3V01QNgSuIhJUiVH5KAWbzT7UaMTHdXynhSLvih9mxxt+iK7wvSS4Ns5++nxWFYJdWEyOK65Nda5l3knKnNnES0azrYeXI4Z+MEfuomxnchW8rJMXXl2rGpgw1vI9Mn6t4CuOmxUaCNDr+oa+xxkS+6035PGlc8PRaYDTaLqZoH8Z0xIRGNj4PHcAr2PJVAUC+vm3Yc6OnaKD/g4LObiHSAlPmlEfX6PIuxeRciakn91YYkg1IzcnsRLN0jhQATWh/nYvs9m1KUALyxVn6O05VF6x5Ikfx8uSc53D1rRm/HowGIXbV7ql91TEvvkrjicXAr+JzO4aPkBU2c1c270QIzv4TPpL9eeuyy8BJgyS0tBUlsPBzLP8tgtuAE7h90sfIYhaS7MIew4x2vh0ZPttdbmxvv8EDCzN1mUAlvMYxxPk5aanKS+6a/dvBiQlIUw73lnbfZ6hh2qNTP6Gq0sN31S+yYrr3c408abKYehNMYX7g2gGc9wQ== root@flaketorio";
  users = [ niklash matias tuukka flaketorio ];
in {
  "password.age".publicKeys = users;
  "token.age".publicKeys = users;
}
