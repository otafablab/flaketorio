{
  description = "Flaketorio";

  inputs = {
    nixpkgs.url = "github:nixos/nixpkgs/nixos-23.05";

    agenix.url = "github:ryantm/agenix";
  };

  outputs = { self, nixpkgs, agenix }@inputs: {
    nixosConfigurations.flaketorio = nixpkgs.lib.nixosSystem {
      system = "x86_64-linux";
      pkgs = import nixpkgs { overlays = [ (import ./overlays/default.nix) ]; };
      modules =
        [
          ./configuration.nix
          ./factorio.nix
          ./modules
          agenix.nixosModules.default
        ];
    };
  };
}
