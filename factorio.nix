{ config, lib, pkgs, ... }:
let
  specialCases = {
    gameName = "game-name";
    gamePassword = "game-password";
    autosaveInterval = "autosave-interval";
  };
  fixCase = name: value: {
    inherit value;
    name = specialCases."${name}" or name;
  };
  # modDir = /home/niklash/.factorio/mods;
  modDir = /mods;
  # modToDrv = name_:
  #   pkgs.runCommand "copy-factorio-mods" {} ''
  #     mkdir -p $out
  #     cp "${modDir + "/${name_}"}" $out/"${name_}"
  #   '' // { deps = []; };
  # modList = lib.pipe modDir [
  #   builtins.readDir
  #   (lib.filterAttrs (k: v: v == "regular"))
  #   (lib.mapAttrsToList (k: v: k))
  #   (builtins.filter (lib.hasSuffix ".zip"))
  # ];
in {
  config.services.flaketorio = {
    enable = true;
    admins = [ "niklash" "Pingnu" "TuuKeZu" "maseh" ];
    description = "Flaketorio";
    gameName = "Flaketorio";
    gamePassword = lib.removeSuffix "\n"
      (builtins.readFile config.age.secrets.password.path);
    token = builtins.readFile config.age.secrets.token.path;
    saveName = "16152012015";
    modDirImpure = modDir;
  };
}
