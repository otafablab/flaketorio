{ config, pkgs, ... }: {
  imports = [ # Include the results of the hardware scan.
    ./hardware-configuration.nix
  ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.efiSupport = false;
  boot.loader.grub.device = "/dev/sda"; # or "nodev" for efi only

  networking.hostName = "flaketorio"; # Define your hostname.
  networking.networkmanager.enable =
    true; # Easiest to use and most distros use this by default.

  time.timeZone = "Europe/Helsinki";

  users.users.admin = {
    isNormalUser = true;
    extraGroups = [ "wheel" ]; # Enable ‘sudo’ for the user.
    openssh.authorizedKeys.keys = [
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC+O2c5XNryJUCKs5glP2GvfihpNKEWxRyxV0mG4xjDpqRQU7lA8/g0uGWqMXqfqGpRORtNHgmA4cIiWBC7Qv6LA4tYPXYPPIPDipcG7FPTu3d2K6PXD2/RF9KOHl1LIxgzk6ExEMYSW8CYXnawJNvjQFK1naR1+TlmQO5K4lBgyXapBBBjLQeASsMoVQXhhrHgh8cKvRWfFyoicV43K0K/r9x/8UarzNQ8imI4JB1F4tpWjn8tzMGfV1wLhyy2pO2OjkOI77oZSEwquuKZpw54ybBsnTpXO5a1wDVtEESXn44PWYGneaZCPUoSmBlcGS66rioN7+2KgnMQ16+7cT3dQoiHgFBhJv6aimUAqZC+jhlPFnWeNhMtQa9dBGqXMX6g3Z3EoKNcvGac6mW0r/LjK3WsyYtZpFQJ109FbtqesWyYlPVmIkGeHtNuCeEkj5QF/qaql1J4znigTp4XPSZoAEXW5nQq20apjZG65zfAMaSbZK9u8jH4+k4aZuoKgVk= niklash@Z370"
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDer2ocWNu66Fa6nkFfv7a0kwxFPfx+Yw9WMEA8awRZjBvj9gMdkGcL95jWcLclQT3yielsLgsTZ6cz543NF2mxI1d/NRenazWJFCikpORq3RfQyuMpnif8mpUej6hwVndkR9R0CLBEDHclgHOLxK7Xq8dCHUC2yqNtL59G1bdrV0gRX0aV2b/hFZiMhVb0Y1cCI1qP1V4eALM83L/9Vho5JMJsmo89SIQ0DXXIo1VRL6IrIaQ+/TOUKcfyrJvfvj+htSMG08lst8DIfTIVghUmRqqmoVOr7wBPiML0+hT+3ElT2+MrnmW1TfnITD7Yc3LV+WZ3LHa18cZ45cEbwOfwJd5ve4SeIcjW8EQZTAxWoGQLyNQTuPcDUpiD91U3kSUz5+w/bzRrAk/7YzA9goMbLJm4hUXIyqBJ0o4EWOIR5QledThuih7WE3GSQOurpjDJqR7p6e02dp4OpLulnvKtIfEB2HQ6Ajpbsexs44i/E2cwxYRINvYGOnQvL2o14Lc= Niklas Halonen (gitlab.com)"
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDLNW9Fq02urGJrZgTL7+oz8wDx+w89QQBpHk1S0sQRsBiOGMhjmpmMZ506KEKO5/4qUd/Kn4HkBhNPu8faKHrwz1hNa+qUZdj+36FjCGLYvP95DUvHqgaP4diKpI/eS0PGVxaLqDt+UFFAdj84zVCGt6wmbENqENAMaapJvMHeuaB6KKARWkegUcet8kjNDIslYTeAHobQqec57Jyd57yUKKgGjDGY6T1Wrwpo1gF84wcaLk17sGaXmHBjSbeeqKg4jBgcPHkaAKh+GV7pqkNbovinQEZxCGKlaLqmCZvZnwAFeY4TFUW3ztuwkI75YsalQq2ZZ1rfvMn+CMatJXSFtNi+skwIktsb0+VoRKYfD4lFVwhcxzxvjIwY99l1tJX+IuYXO83SFcg1Lz2+3Plhc9fGwFuWgU1N0h/EzE0u9yVnPY9ARWuSLWqfOEOPrGxtvIWg6RsVF5/yr9IKX7YS4NzgybgQiaycOzuNXiOKYFpHBJ2O/wllQ9XNOS1ysoU= Niklas Halonen (gitlab.com)"
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCnNFXgY2vxk5Cp7pOUtHDSeY7S1v5ucTsw3hfJn7yR7R/uKh/s7aDT74vPbLHCw9uLd0/lqUcam609ItUid3r090SpJ02oe/rv2pJjEoGzEAZb7nVHGtkNaWE+palBmwAUPN33AVaHebPtbBVlaSu6cKDmA1ngc7pYdVHT9yMDSGOdIxxNO5nXnu+VyZyOReA6ZBaTWb/ALs65m5x0667lT9C+RGiUDjUaVmaxdqD4mCD21Fu2LyxqYrlkxU9gzCrEWQ6l+s2skqXBGpB2MXMbujR6elMgJ2gJmR8n1pwxlF2pEI7xCEF6aWv3hVHnSi/1qzofHqJeuQ8VdL1VekojzGVZj+z60833+UB+H/D0W7pes5HJ3A5e8/n2g5pohuPTta0Uv9dFnOPGwkC0ATtgvoS1HmE93GCzy/Mz0ogbjO6SnivXW1IYMcO3qBu1o9wPRVUK5IM6y9xfC+PnpZHEozRHaaLcldojQ4CEvNUYCmtONJ5jUoyKzzKVBNGehcU= Niklas Halonen (gitlab.com)"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKVXV+51+7Evucq9Qi9QCs2LugQii6AjvDfIg3u7oiOe"
      "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQC2c/FQ9M+4UXPSbwBNysLBPgkMI3Lsa1SBLlaOCE5oT4jhc1/oy1h9C/D8gkrnvmbGxpa8XAsp54a7nXWCWyVJBvX6VKFZ8EN5q7AciA0vtxOPYxwOsL9BWEZRfYcH2Eubz+cxntQcmGwuPTp95Jz0MMgOm7saUVTMujSCkFclQtpedAWb5KO+Wp3gu4PTBT3JV2Os5OiLxsnsgvyvBU23lB74b1vqIAwZS7skTS90T03AEenwR1kBeoBaMeadITvd18DDkvu5sAc/vSaXToXh+TkS1NXsWW356h1D2qrLjiveQFRXhqrgWrNhYI1au6ncC1XrmOSLVt2WGjhicYWLDKHD9M/WiKwAKuJnK8hK1OANFazKxJKSp32w40d5u+SpzkI+HUTkrvTDcERv9rwlIf3aN9+LZ6WPlnbYJie3FW/RfhoUA/fPxEtKQ+iMKMo4Id5L9VoH7dEIVawkaH9G+9mqVklDAMHo51BPoWiTkPWT2Kb4q5x7WGyV1eSQKvlgeDvEO+NANRuSfOnV4D9URdR1FHjykxqeK624Vg25SJ414acueYRgEOmnlkPtCNTN7P/Uj1EF4RAaj0EevT3pN2TeqDNHMUV2WBWNBsWW/t/dU6gJ7k4aiQ9hNxLqIKHDoiKBBus6Ri/0vDnHG8i3QfacF+t0ZnoSUTeJ7QJpUw== tuukk@desktop-tuukk"
    ];
  };

  nixpkgs.config.allowUnfree = true;

  environment.systemPackages = with pkgs; [
    emacs # Do not forget to add an editor to edit configuration.nix! The Nano editor is also installed by default.
    factorio-headless
  ];

  age.secrets.password.file = ./secrets/password.age;
  age.secrets.token.file = ./secrets/token.age;

  # Enable the OpenSSH daemon.
  services.openssh.enable = true;
  services.openssh.settings.PasswordAuthentication = false;

  networking.firewall = {
    enable = true;
    allowedUDPPorts = [
      34197 # factorio
    ];
  };

  nix = {
    gc = {
      persistent = true;
      automatic = true;
      dates = "weekly";
      options = "--delete-older-than 14d";
    };
    settings.keep-outputs = true;
    settings.keep-derivations = true;
    settings.experimental-features = [ "nix-command" "flakes" ];
    
    settings = {
      auto-optimise-store = true;
    };
  };

  system.stateVersion = "23.05"; # Did you read the comment?
}

